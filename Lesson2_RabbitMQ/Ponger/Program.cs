﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ponger
{
    class Program
    {
        private static int delay = 2500;

        static void Main(string[] args)
        {

            using var _rabbit = new RabbitWrapper("test", "ping_queue", "ping");
            _rabbit.ListenQueue();
            _rabbit.MessageReceived += Rabbit_MessageReceived;

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();



        }

        
        private static void Rabbit_MessageReceived(object sender, BasicDeliverEventArgs e)
        {

            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            SendToConsole($"{DateTime.Now} received message: {message}", ConsoleColor.Green);

            SendResponseMessage();



        }

        private static void SendToConsole(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }


        private static void SendResponseMessage()
        {
            Thread.Sleep(delay);

            using var _rabbit = new RabbitWrapper("test", "pong_queue", "pong");

            _rabbit.SendMessageToQueue("pong");

            SendToConsole($"{DateTime.Now} send message: pong", ConsoleColor.Yellow);


        }

    }
}
