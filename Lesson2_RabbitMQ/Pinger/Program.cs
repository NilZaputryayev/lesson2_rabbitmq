﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;


namespace Pinger
{
    class Program
    {
        private static int delay = 2500;
        private static DateTime lastResponse;
        private static System.Timers.Timer responseRecoveryTimer;

        static void Main(string[] args)
        {
            Thread.Sleep(2000);

            responseRecoveryTimer = new System.Timers.Timer(10000);
            responseRecoveryTimer.Elapsed += ResponseRecoveryTimer_Elapsed;
            responseRecoveryTimer.Start();

            SendMessage();

            var _rabbit = new RabbitWrapper("test", "pong_queue", "pong");
            _rabbit.ListenQueue();
            _rabbit.MessageReceived += Rabbit_MessageReceived;

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();


        }

        private static void ResponseRecoveryTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (DateTime.Now - lastResponse > TimeSpan.FromMilliseconds(responseRecoveryTimer.Interval)) 
                SendMessage();
        }

        private static void SendMessage()
        {

            using var _rabbit = new RabbitWrapper("test", "ping_queue", "ping");

            _rabbit.SendMessageToQueue("ping");
            SendToConsole($"{DateTime.Now} sent ping", ConsoleColor.Yellow);
            lastResponse = DateTime.Now;


        }

        private static void Rabbit_MessageReceived(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            SendToConsole($"{DateTime.Now} received message: {message}", ConsoleColor.Green);
            Thread.Sleep(delay);
            SendMessage();



        }

        private static void SendToConsole(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
