﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper
{
    public class RabbitWrapper : IDisposable
    {
        private protected string _exchange;
        private protected string _queue;
        private protected string _key;
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channel;
        private EventingBasicConsumer _consumer;

        public RabbitWrapper(string exchange, string queue, string key)
        {
            _exchange = exchange;
            _queue = queue;
            _key = key;
            ConfigRabbit();
            
        }

        private void ConfigRabbit()
        {
            do
            {
                try
                {
                    _factory = new ConnectionFactory() { HostName = "localhost" };
                    _connection = _factory.CreateConnection();
                    _channel = _connection.CreateModel();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error init connection, reason: {ex.Message}");
                    Thread.Sleep(1000);
                }
            } while (true);
        }

        public event EventHandler<BasicDeliverEventArgs> MessageReceived;

        public void ListenQueue()
        {
            if (_channel == null) ConfigRabbit();

                _channel.ExchangeDeclare(_exchange, ExchangeType.Direct);

                _channel.QueueDeclare(queue: _queue,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                _channel.QueueBind(_queue, _exchange, _key);

                _consumer = new EventingBasicConsumer(_channel);

                _consumer.Received += Consumer_Received;

                _channel.BasicConsume(queue: _queue,
                                     autoAck: true,
                                     consumer: _consumer);

                Console.WriteLine($"Message listener for {_queue} started");

        }

        public void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            MessageReceived?.Invoke(this, e);
        }




        public void SendMessageToQueue(string message)
        {
            if (_channel == null) ConfigRabbit();
            
                _channel.ExchangeDeclare(_exchange, ExchangeType.Direct);

                _channel.QueueDeclare(queue: _queue,
                                      durable: false,
                                      exclusive: false,
                                      autoDelete: false,
                                      arguments: null);


                var body = Encoding.UTF8.GetBytes(message);

                _channel.BasicPublish(exchange: _exchange,
                                     routingKey: _key,
                                     basicProperties: null,
                                     body: body);
           
        }

        public void Dispose()
        {
            _channel?.Close();
            _channel?.Dispose();
            _connection?.Close();
            _connection?.Dispose();
        }
    }
}
